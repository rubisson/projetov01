package utfpr.ct.dainf.if62c.projeto;

import java.io.Serializable;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto extends Object implements Serializable{
    private double x, y, z;
    
    public Ponto(){
        this(0,0,0);
    }

    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        System.out.println(getClass());
        return getClass().getSimpleName()+String.format("(%1$f,%2$f,%3$f)",x,y,z);
    }
    
    @Override
    public boolean equals(Object obj){
        /* se o argumento for diferente de null e for uma instância
        da classe Ponto a condição será verdadeira  */
        /*    if (obj!=null && obj instanceof Ponto){
        System.out.println("deu certo");
        return true;
        }else{
        System.out.println("Pontos Diferentes");
        return false;
        }
        //falta terminar!
         */
        return this==obj;
    }
    
    
    public double dist(Ponto obj){
        return Math.sqrt(Math.pow(this.x-obj.getX(),2)+Math.pow(this.y-obj.getY(),2)+Math.pow(this.z-obj.getZ(),2));
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Rubinho
 * @param <T>
 */
public class PoligonalFechada <T extends Ponto> extends Poligonal{
    private T[] vertices;
    public PoligonalFechada() {
    }

    public PoligonalFechada(T[] vertices) {
        super(vertices);
        this.vertices = vertices;
    }

    @Override
     public double getComprimento(){
        return super.getComprimento()+vertices[0].dist(vertices[vertices.length-1]);
    }
}

import java.text.NumberFormat;
import java.util.Locale;
import utfpr.ct.dainf.if62c.projeto.PoligonalFechada;
import utfpr.ct.dainf.if62c.projeto.Poligonal;
import utfpr.ct.dainf.if62c.projeto.PontoXY;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;
import utfpr.ct.dainf.if62c.projeto.PontoYZ;



/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        PontoXZ obj1 = new PontoXZ(-3,2);
        PontoXZ obj2 = new PontoXZ(-3,6);
        PontoXZ obj3 = new PontoXZ(0,2);
        PontoXZ[] vertices = new PontoXZ[3];
        vertices[0] = obj1; vertices[1] = obj2; vertices[2] = obj3;
        //Poligonal<PontoXZ> objt1 = new Poligonal<>(vertices);
        
        PoligonalFechada<PontoXZ> objt2 = new PoligonalFechada<>(vertices);
        Locale l = new Locale("pt","BR");
        NumberFormat nf = NumberFormat.getInstance(l);
        
       // System.out.println(objt1.getN());
        //System.out.println("O comprimento da poligonal = " + objt1.getComprimento());
        
        //System.out.println(objt2.getN());
        System.out.println("O comprimento da poligonal = " + nf.format(objt2.getComprimento()));
        //System.out.println("são iguais ou não: " + obj1.equals(obj1));
    }
}
